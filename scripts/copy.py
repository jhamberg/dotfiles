"""
Used to copy external repos to remote host as they are not included in the
archive.
"""

import argparse
import os
import toml


def main():
    """
    Run code to copy external repos to remote host.
    """

    parser = argparse.ArgumentParser(
                    prog='ProgramName',
                    description='What the program does',
                    epilog='Text at the bottom of help')
    parser.add_argument('host')
    parser.add_argument('--dry', default=False)
    args = parser.parse_args()

    host = args.host
    dry = args.dry
    data = toml.load('.chezmoiexternal.toml')
    for k in data.keys():
        dir_cmd = f'ssh {host} mkdir -p ~/{k}'
        copy_cmd = f'rsync -ra ~/{k}/ {host}:~/{k}/'
        print(dir_cmd)
        if not dry:
            os.system(dir_cmd)
        print(copy_cmd)
        if not dry:
            os.system(copy_cmd)


if '__main__' == __name__:
    main()
