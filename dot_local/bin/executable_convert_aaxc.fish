#!/usr/bin/env fish
for i in *aaxc
    set b (basename $i .aaxc)
    set voucher $b.voucher
    set key (cat $voucher | jq ".content_license.license_response.key" --raw-output)
    set iv (cat $voucher | jq ".content_license.license_response.iv" --raw-output)
    echo ffmpeg -activation_bytes f51cbd08 -audible_key $key -audible_iv $iv -i $i -c copy $b.m4b
    ffmpeg -activation_bytes f51cbd08 -audible_key $key -audible_iv $iv -i $i -c copy $b.m4b
end

