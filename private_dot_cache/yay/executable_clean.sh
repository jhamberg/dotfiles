#!/usr/bin/env fish
rm -rf **.zip
rm -rf **.pkg.tar.zst
rm -rf **.rpm
rm -rf **.tar.gz
rm -rf **.iso
rm -rf **.tgz
rm -rf **.pak
rm -rf **.deb
rm -rf **.tar.xz
rm -rf **.pkg.tar
rm -rf **.tar.bz2
rm -rf **.run
rm -rf */src
rm kindlegen/kindlegen
rm -rf stlink-git/stlink-git

for d in */
    cd $d
    git clean -fd
    cd ..
end
