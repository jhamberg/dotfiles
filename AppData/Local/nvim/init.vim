" Autoload {{{

let homeDir = expand('~')
if(has('win32') || has('win64'))
    let $TMP='c:/tmp'
    if has('nvim')
        let shareDir=homeDir.'\AppData\Local\nvim'
        let plugVim=shareDir.'\autoload\plug.vim'
    else
        let shareDir=homeDir.'\vimfiles'
        let plugVim=shareDir.'\autoload\plug.vim'
    endif
else
    if has('nvim')
        let shareDir=homeDir.'/.local/share/nvim/site'
        let plugVim=shareDir.'/autoload/plug.vim'
    else
        let shareDir=homeDir.'/.vim'
        let plugVim=shareDir.'/autoload/plug.vim'
    endif
endif

let plugUri = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
if empty(glob(expand(plugVim)))
    " Make the symbolic link for gVim on Windows.
    if has('win32') || has('win64')
        exec '!md "'.shareDir.'\backup_files"'
        exec '!md "'.shareDir.'\swap_files"'
        exec '!md "'.shareDir.'\undo_files"'
        exec '!md "'.shareDir.'\autoload"'

        exec '!powershell -command Invoke-WebRequest -Uri '.plugUri.' -OutFile ("'''.plugVim.'''")'

    else
        exec '!mkdir -p '.shareDir.'/{backup_files,swap_files,undo_files}'

        " Create command to download plug manager.
        exec '!curl -fLo '.plugVim.' --create-dirs '.plugUri
    endif

    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" }}}
" Plug {{{

call plug#begin(shareDir.'/plugged')

Plug 'Chiel92/vim-autoformat' " clang-format suuport
Plug 'mhinz/vim-signify'
Plug 'glts/vim-magnum' " Change base of numbers
Plug 'glts/vim-radical' " Change base of numbers.
Plug 'junegunn/fzf' " Fuzzy file finder.
Plug 'junegunn/fzf.vim' " Fuzzy file finder.
Plug 'ludovicchabant/vim-gutentags' " ctags support
Plug 'morhetz/gruvbox' "Color support.
Plug 'qpkorr/vim-bufkill' " Kill buffer support.
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'} " File browser.
Plug 'sheerun/vim-polyglot' " Defaults for a bunch of languages.
Plug 'tpope/vim-commentary' " Comment keybinging.
Plug 'tpope/vim-fugitive' " Git in vim.
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-surround'
Plug 'justinmk/vim-dirvish'
Plug 'rbong/vim-crystalline'

Plug 'vimwiki/vimwiki', {'on': ['VimwikiIndex', 'VimwikiMakeDiaryNote']} " Wiki in vim.


call plug#end()
" }}}
" Plugin Configuration {{{
"
" FZF {{{
" File Finder
noremap <C-p> :Files<CR>
"}}}
" Nerdtree {{{
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
" }}}
" Misc {{{

" Setup common directory for writing backup, swap, and undo files.
let &backupdir=shareDir.'/backup_files//'
let &directory=shareDir.'/swap_files//'
let &undodir=shareDir.'/undo_files//'


" Common Type Correction
ca Pwd pwd
ca q1 q!

" Disable VI compatibility
set nocompatible

" Allow the ability to switch buffers without saving the buffer.
set hidden

" Default location of VimWiki.
let g:vimwiki_list = [{'path': '~/OneDrive/vimwiki/',  'syntax': 'markdown', 'ext': '.md', 'list_margin':0}]

set notimeout

" Choose a better looking vertical seperator for terminal vim.
if !has('win32') && !has('win64')
    set fillchars=vert:│    " that's a vertical box-drawing character
endif

" Disable the Ex mode.
nnoremap Q @q

" Enable syntax highlighting.
syntax on

let g:tex_conceal = ""

" Visualize autocomplete for command menue
set wildmenu

" redraw only when we need to.
set lazyredraw

" highlight matching [{()}]
set showmatch

" Show line numbers.
set number

" search as characters are entered
set incsearch

" highlight matches
set hlsearch

set laststatus=2
set wildignore+=*\\tmp\\*,*.swp,*.swo,*.zip,.git,.cabal-sandbox

" Use the system clipboard on windows.
if has('win32') || has('win64')
    set clipboard=unnamed
else
    set clipboard+=unnamedplus
endif

" Move vertically by visual line.
nnoremap j gj
nnoremap k gk


" AutoFormat.
nnoremap <C-A-l> :Autoformat<CR>

" 80 character warning line
set colorcolumn=80
set linebreak


" }}}
" Spaces & Tabs {{{

" Configure 4 space tabs.
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4
set smarttab

" Allow the backspace to work.
set backspace=indent,eol,start

" Allow mouse support in the terminal.
set mouse=a
if !has('nvim')
    set ttymouse=xterm2
endif

" }}}
" UI Layout {{{

" Set the gruvbox colorscheme.
colorscheme gruvbox
set background=dark

if has("gui_running")
    if has('win32') || has('win64')
        " Set the font on windows.
        set guifont=Consolas:h11:cDEFAULT " Set the font.
    else
        " Set the font on Linux.
        set anti enc=utf-8
        set guifont=FiraCode\ 8
    endif

    "  Make  GUI vim look more like terminal Vim.
    " remove the menu bar
    set guioptions-=m
    " remove the toolbar
    set guioptions-=T
    " remove the right-hand scroll bar
    set guioptions-=r
    " remove the left-hand scroll bar
    set guioptions-=L
endif

" }}}
" Folding {{{

setlocal foldmethod=marker
setlocal foldlevel=0
setlocal modelines=1

" }}}
" Leader Shortcuts {{{

" Set the leader key.
let mapleader=" "

" Find in directory.
nnoremap <leader>s :Rg<CR>

" Delete buffer shortcut.
" nnoremap <leader>w :BD<cr>
nnoremap <leader>w<leader>w :VimwikiMakeDiaryNote<CR>
nnoremap <leader>ww :VimwikiIndex<CR>

" Nerd tree toggle shortcut.
nnoremap <Leader>f :NERDTreeToggle<Enter>

" Easier buffer navigation.
noremap <leader>n :bn<CR>
noremap <leader>p :bp<CR>
noremap <leader>d :put =strftime('%a %d %b %Y')<CR>

" Incremental search highlighting.
nnoremap <leader><space> :nohlsearch<CR>

" }}}
" Mappings {{{
" Strip whitespace.
noremap <F3> :Autoformat<CR>
noremap <F5> :StripWhitespace<CR>

inoremap <c-d> <esc>ddi
inoremap <c-u> <esc>viwUi
" }}}
" Languages {{{
au BufNewFile,BufRead,BufReadPost *.v set syntax=verilog

" Make sure vh are recognized as verilog files.
au BufRead,BufNewFile *.vh set filetype=verilog

" Python {{{
let python_highlight_all=1
set encoding=utf-8
" }}}
" }}}

" FormatXML command.
com! FormatXML :%!python3 -c "import xml.dom.minidom, sys; print(xml.dom.minidom.parse(sys.stdin).toprettyxml())"
com! FormatJSON %!python -m json.tool

function! StatusLine(current, width)
    let l:s = ''

    if a:current
        let l:s .= crystalline#mode() . crystalline#right_mode_sep('')
    else
        let l:s .= '%#CrystallineInactive#'
    endif
    let l:s .= ' %f%h%w%m%r '
    if a:current
        let l:s .= crystalline#right_sep('', 'Fill') . ' %{fugitive#head()}'
    endif

    let l:s .= '%='
    if a:current
        let l:s .= crystalline#left_sep('', 'Fill') . ' %{&paste ?"PASTE ":""}%{&spell?"SPELL ":""}'
        let l:s .= crystalline#left_mode_sep('')
    endif
    if a:width > 80
        let l:s .= ' %{&ft}[%{&fenc!=#""?&fenc:&enc}][%{&ff}] %l/%L %c%V %P '
    else
        let l:s .= ' '
    endif

    return l:s
endfunction

function! TabLine()
    let l:vimlabel = has('nvim') ?  ' NVIM ' : ' VIM '
    return crystalline#bufferline(2, len(l:vimlabel), 1) . '%=%#CrystallineTab# ' . l:vimlabel
endfunction

let g:crystalline_enable_sep = 1
let g:crystalline_statusline_fn = 'StatusLine'
let g:crystalline_tabline_fn = 'TabLine'
let g:crystalline_theme = 'gruvbox'

set showtabline=2
set guioptions-=e
set laststatus=2

" vim:foldmethod=marker:foldlevel=0
