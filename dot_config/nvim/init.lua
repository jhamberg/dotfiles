
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = ' '

vim.opt.termguicolors = true -- Enable 24-bit RGB colors

require("lazy").setup({
    {
        'ellisonleao/gruvbox.nvim',
        config = function ()
            require("gruvbox").setup({
                contract = "soft"
            })
            vim.cmd([[colorscheme gruvbox]])
        end,
    },
  -- fzf extension for telescope with better speed
  {
    "nvim-telescope/telescope-fzf-native.nvim", run = 'make' 
  },
  {
    "jvgrootveld/telescope-zoxide"
  },
  {
    "vhyrro/luarocks.nvim",
    priority = 1000, -- Very high priority is required, luarocks.nvim should run as the first plugin in your config.
    config = true,
  },
  {'nvim-telescope/telescope-ui-select.nvim' },
  -- file explorer
  {
    "nvim-tree/nvim-tree.lua",
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    -- lazy = true,
    keys = {
      {"<leader>n", ":NvimTreeToggle<cr>", desc = "NvimTree" },
    },
    config = function()
      require("nvim-tree").setup({

        sync_root_with_cwd = true,
        sort_by = "case_sensitive",
        filters = {
          dotfiles = true,
        },
        on_attach = function(bufnr)
          local api = require('nvim-tree.api')

          local function opts(desc)
            return {
              desc = 'nvim-tree: ' .. desc,
              buffer = bufnr,
              noremap = true,
              silent = true,
              nowait = true,
            }
          end

          api.config.mappings.default_on_attach(bufnr)

          vim.keymap.set('n', 's', api.node.open.vertical, opts('Open: Vertical Split'))
          vim.keymap.set('n', 'i', api.node.open.horizontal, opts('Open: Horizontal Split'))
          vim.keymap.set('n', 'u', api.tree.change_root_to_parent, opts('Up'))
        end
      })
    end,
  },

  -- fuzzy finder framework
  {
    "nvim-telescope/telescope.nvim", 
   keys = {
      -- add a keymap to browse plugin files
      -- stylua: ignore
      {
        "<leader>ff",
        "<cmd>:Telescope<cr>",
        desc = "Open telescope",
      },
      {
        "<leader>fp",
        "<cmd>:Telescope find_files<cr>",
        desc = "Find Plugin File",
      },
      {
        "<leader>fs",
        "<cmd>:Telescope live_grep<cr>",
        desc = "Live Grep",
      },
      {
        "<leader>fb",
        "<cmd>:Telescope buffers<cr>",
        desc = "Buffer search",
      },
      {
        "<leader>fz",
        "<cmd>:Telescope zoxide list<cr>",
        desc = "Zoxide search",
      },
    },
    dependencies = { 
      "nvim-lua/plenary.nvim" ,
      "nvim-treesitter/nvim-treesitter",
      "nvim-tree/nvim-web-devicons",
    },
    config = function ()
      require("telescope").setup({
        extensions = {
          fzf = {
            fuzzy = true,                    -- false will only do exact matching
            override_generic_sorter = true,  -- override the generic sorter
            override_file_sorter = true,     -- override the file sorter
            case_mode = "smart_case",        -- or "ignore_case" or "respect_case"
          },
          zoxide = {
            prompt_title = "[ Walking on the shoulders of TJ ]",
            mappings = {
              default = {
                after_action = function(selection)
                  print("Update to (" .. selection.z_score .. ") " .. selection.path)
                end
              },
              ["<C-s>"] = {
                before_action = function(selection) print("before C-s") end,
                action = function(selection)
                  vim.cmd.edit(selection.path)
                end
              },
              -- Opens the selected entry in a new split
              -- ["<C-q>"] = { action = z_utils.create_basic_command("split") },
            },
          },
      }})

      -- To get fzf loaded and working with telescope, you need to call
      -- load_extension, somewhere after setup function:
      require('telescope').load_extension('fzf')

      -- To get ui-select loaded and working with telescope, you need to call
      -- load_extension, somewhere after setup function:
      require("telescope").load_extension("ui-select")

      require("telescope").load_extension('zoxide')

    end,
  },
  { 'mhinz/vim-signify'},
  { 'glts/vim-magnum'},
  { 'glts/vim-radical'},
  { 'qpkorr/vim-bufkill'},
  { 'tpope/vim-commentary'},
  { 'tpope/vim-fugitive'},
  { 'tpope/vim-vinegar'},
  { 'tpope/vim-surround'},
  { 'tpope/vim-dispatch'},
  { 'sindrets/diffview.nvim'},
  { 'justinmk/vim-dirvish'},
  { "nvchad/volt" , lazy = true },
  { "nvchad/menu" , lazy = true },
  { 'vim-scripts/LargeFile'},
  { 'neovim/nvim-lspconfig'},
  { 'ms-jpq/coq_nvim'},
  { 'pechorin/any-jump.vim'},
  {
    'nvim-lualine/lualine.nvim',
    config = function()
      require('lualine').setup()
    end,
  },
  { 'rbong/vim-crystalline'},
  { 'vim-scripts/argtextobj.vim'},
  { 'michaeljsmith/vim-indent-object'},
  {
    'akinsho/bufferline.nvim',
    version = "*",
    dependencies = 'nvim-tree/nvim-web-devicons',
    config = function ()
      require("bufferline").setup{}
    end,
  },
})
local lspconfig = require('lspconfig')
local coq = require "coq" -- add this

lspconfig.clangd.setup {
    function()	
    	vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
    	vim.keymap.set("n", "gD", function() vim.lsp.buf.declaration() end, opts)
    	vim.keymap.set("n", "gt", function() vim.lsp.buf.type_definition() end, opts)
    	coq.lsp_ensure_capabilities()
    end
}

-- require("menu").open(options, opts) 
-- Keyboard users
vim.keymap.set("n", "<C-m>", function()
  require("menu").open("default")
end, {})

-- mouse users + nvimtree users!
vim.keymap.set("n", "<RightMouse>", function()
  vim.cmd.exec '"normal! \\<RightMouse>"'

  local options = vim.bo.ft == "NvimTree" and "nvimtree" or "default"
  require("menu").open(options, { mouse = true })
end, {})

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.opt.termguicolors = true -- Enable 24-bit RGB colors

vim.opt.number = true -- Show line numbers.
vim.opt.showmatch = true -- Highlight matching parenthesis.

vim.opt.tabstop=4
vim.opt.expandtab=true
vim.opt.shiftwidth=4
vim.opt.autoindent=true
vim.opt.smartindent=true
-- vim.opt.cindent=true
vim.opt.cinkeys:remove { "0#" }
vim.opt.indentkeys:remove { "0#" }
vim.opt.cindent=true
vim.opt.copyindent=true

vim.opt.showtabline=2
-- vim.opt.guioptions-=e
vim.opt.laststatus=2
vim.opt.mouse=a


vim.api.nvim_set_keymap('n', '<leader>gu', ':SignifyHunkUndo<cr>', { noremap = true })
vim.api.nvim_set_keymap('n', '<leader>gj', '<plug>(signify-next-hunk)', {})
vim.api.nvim_set_keymap('n', '<leader>gk', '<plug>(signify-prev-hunk)', {})

local home_dir = vim.fn.expand('~')
local share_dir
local plug_vim

if vim.fn.has('win32') == 1 or vim.fn.has('win64') == 1 then
  vim.env.TMP = 'c:/tmp'
  if vim.fn.has('nvim') == 1 then
    share_dir = home_dir .. '/AppData/Local/nvim'
    plug_vim = share_dir .. '/autoload/plug.vim'
  else
    share_dir = home_dir .. '/vimfiles'
    plug_vim = share_dir .. '/autoload/plug.vim'
  end
else
  if vim.fn.has('nvim') == 1 then
    share_dir = home_dir .. '/.local/share/nvim/site'
    plug_vim = share_dir .. '/autoload/plug.vim'
    packer_vim = share_dir .. '/pack/packer/start/packer.nvim/'
  else
    share_dir = home_dir .. '/.vim'
    plug_vim = share_dir .. '/autoload/plug.vim'
  end
end

-- Nerdtree
vim.g.NERDTreeMinimalUI = 1
vim.g.NERDTreeDirArrows = 1

-- Misc
vim.opt.backupdir = share_dir .. '/backup_files//'
vim.opt.directory = share_dir .. '/swap_files//'
vim.opt.undodir = share_dir .. '/undo_files//'

-- vim.opt.nocompatible = true
vim.opt.list = true

vim.opt.hidden = true
vim.opt.timeout = false

if vim.fn.has('win32') ~= 1 and vim.fn.has('win64') ~= 1 then
  vim.opt.fillchars = 'vert:│'
end

-- vim.nnoremap('Q', '@q')

vim.cmd [[syntax on]]

vim.g.tex_conceal = ''

vim.opt.wildmenu = true

vim.opt.lazyredraw = true

vim.opt.number = false

vim.opt.incsearch = true
vim.opt.hlsearch = true

vim.opt.laststatus = 2
-- vim.opt.wildignore = vim.opt.wildignore + { '*\\tmp\\*', '*.swp', '*.swo', '*.zip', '.git', '.cabal-sandbox' }

if vim.fn.has('win32') == 1 or vim.fn.has('win64') == 1 then
  vim.opt.clipboard = 'unnamed'
else
  vim.opt.clipboard = vim.opt.clipboard + 'unnamedplus'
end

vim.api.nvim_set_keymap('n', 'j', 'gj', {})
vim.api.nvim_set_keymap('n', 'j', 'gj', {})

vim.opt.colorcolumn = '80'
vim.opt.linebreak = true

vim.opt.updatetime = 100

vim.opt.mouse = 'a'
if vim.fn.has('nvim') ~= 1 then
  vim.opt.ttymouse = 'xterm2'
end

-- vim.cmd [[colorscheme gruvbox]]
-- vim.opt.background = 'dark'

if vim.fn.has('gui_running') == 1 then
  if vim.fn.has('win32') == 1 or vim.fn.has('win64') == 1 then
    vim.opt.guifont = 'Consolas:h11:cDEFAULT'
  else
    vim.opt.guifont = 'FiraCode 8'
    vim.opt.encoding = 'utf-8'
  end

  vim.opt.guioptions = vim.opt.guioptions - 'm'
  vim.opt.guioptions = vim.opt.guioptions - 'T'
  vim.opt.guioptions = vim.opt.guioptions - 'r'
  vim.opt.guioptions = vim.opt.guioptions - 'L'
end

-- vim.nnoremap('<leader><space>', ':nohlsearch<CR>')

vim.api.nvim_create_autocmd({ 'BufNewFile', 'BufRead', 'BufReadPost' }, {
  pattern = '*.csv',
  callback = function()
    vim.opt.ft = 'none'
  end,
})

vim.g.python_highlight_all = 1
vim.opt.encoding = 'utf-8'
