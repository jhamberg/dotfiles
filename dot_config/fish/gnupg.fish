# Ensure that GPG Agent is used as the SSH agent
set -e SSH_AUTH_SOCK
set -Ux SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)

gpgconf --create-socketdir

# Start or re-use a gpg-agent.
gpgconf --launch gpg-agent

