if status is-interactive
    function l
        ls
    end
    set EDITOR nvim
    set VISUAL nvim
    function v
        $EDITOR $argv
    end
    function t
        to $argv
    end
    function j
        just $argv
    end
    set fish_greeting
    fish_vi_key_bindings
    # source "$HOME/.cargo/env.fish"
    zoxide init --cmd cd fish | source

end


# Added by LM Studio CLI (lms)
set -gx PATH $PATH /home/jonathanhamberg/.cache/lm-studio/bin
